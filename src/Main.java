import java.util.Scanner;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        System.out.println("Guessing game!");
        Random numsRam = new Random();
        int userGuess = numsRam.nextInt(20);
        int tries = 0;

        System.out.println("Enter a number between 1 and 20: ");
        Scanner userInput = new Scanner(System.in);
        int guesser = userInput.nextInt();

        while (guesser != userGuess && tries < 3)  {
            System.out.println("Number not correct. Try again!");
            guesser = userInput.nextInt();
            tries++;
        }
        if (guesser == userGuess) {
            System.out.println("You won!");
        } else {
            System.out.println("Game Over!");
        }
    }

}
